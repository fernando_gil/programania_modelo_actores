package programania.akka.messages;

public class StartPiCalc {
    final int nrOfElements;

    public StartPiCalc(int nrOfElements) {
        this.nrOfElements = nrOfElements;
    }

    public int getNrOfElements() {
        return nrOfElements;
    }
}
