package programania.akka.messages;

public class CalculatePi {
    final int start;
    final int nrOfElements;

    public CalculatePi(int start, int nrOfElements) {
        this.start = start;
        this.nrOfElements = nrOfElements;
    }

    public int getStart() {
        return start;
    }

    public int getNrOfElements() {
        return nrOfElements;
    }
}
