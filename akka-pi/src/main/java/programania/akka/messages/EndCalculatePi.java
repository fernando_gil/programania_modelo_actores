package programania.akka.messages;

public class EndCalculatePi {
    final double value;
    final long millis;

    public EndCalculatePi(double value, long millis) {
        this.value = value;
        this.millis = millis;
    }

    public long getMillis() {
        return millis;
    }

    public double getValue() {
        return value;
    }
}
