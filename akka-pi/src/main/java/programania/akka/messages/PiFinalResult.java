package programania.akka.messages;

public class PiFinalResult {
    final int numElems;
    final double value;
    final long millis;

    public PiFinalResult(int numElems, double value, long millis) {
        this.numElems = numElems;
        this.value = value;
        this.millis = millis;
    }

    public int getNumElems() {
        return numElems;
    }

    public double getValue() {
        return value;
    }

    public long getMillis() {
        return millis;
    }
}
