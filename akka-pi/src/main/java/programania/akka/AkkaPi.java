package programania.akka;

import akka.actor.ActorRef;
import akka.actor.ActorSystem;
import akka.actor.Props;
import com.typesafe.config.Config;
import com.typesafe.config.ConfigFactory;
import com.typesafe.config.ConfigValueFactory;
import programania.akka.actors.Listener;
import programania.akka.actors.Master;
import programania.akka.messages.StartPiCalc;

import java.io.IOException;

public class AkkaPi {

    final static ActorSystem system = ActorSystem.create("calculatePi", createConfig());

    private static Config createConfig() {
        return ConfigFactory.defaultReference()
                .withValue("akka.log-dead-letters-during-shutdown", ConfigValueFactory.fromAnyRef(false));
    }

    public static void main(String[] args) {
        waitForBegin();

        long mainBegin = System.currentTimeMillis();
        for (int i = 0; i < 1E6; i += 1E5) {
            akkaCalculatePiFor(i, system);
        }
        long mainEnd = System.currentTimeMillis();

        System.out.println(String.format("main() ejecutado en %s mseg", mainEnd - mainBegin));
        waitForEnd();
    }

    private static void waitForBegin() {
        try {
            System.out.println("Pulse cualquier tecla para comenzar");
            System.in.read();
        } catch (IOException e) {
            //ignorar
        }
    }

    private static void waitForEnd() {
        try {
            System.out.println("Pulse cualquier tecla para salir");
            System.in.read();
            system.shutdown();
            System.exit(0);
        } catch (IOException e) {
            //ignorar
        }
    }

    public static void akkaCalculatePiFor(int nrOfElements, ActorSystem system) {
        final ActorRef listener = system.actorOf(Props.create(Listener.class));
        final ActorRef master = system.actorOf(Props.create(Master.class, listener));

        master.tell(new StartPiCalc(nrOfElements), ActorRef.noSender());
    }
}
