package programania.akka.actors;

import akka.actor.ActorRef;
import akka.actor.UntypedActor;
import programania.akka.messages.CalculatePi;
import programania.akka.messages.EndCalculatePi;
import programania.akka.messages.NextElement;
import programania.akka.messages.RetrieveValue;

public class Worker extends UntypedActor {

    final ActorRef master;
    final ActorRef persistor;
    long begin;
    long end;
    int currentElement;
    int endElement;

    public Worker(ActorRef master, ActorRef persistor) {
        this.master = master;
        this.persistor = persistor;
    }

    @Override
    public void onReceive(Object o) throws Exception {
        if (o instanceof CalculatePi) {
            CalculatePi msg = (CalculatePi) o;
            begin = System.currentTimeMillis();
            currentElement = msg.getStart();
            endElement = msg.getStart() + msg.getNrOfElements();

            getSelf().tell(new NextElement(), getSelf());

        } else if (o instanceof NextElement) {
            persistor.tell(new RetrieveValue(), getSelf());

        } else if (o instanceof Double) {
            double persistedValue = (double) o;

            if (currentElement == endElement) {
                end = System.currentTimeMillis();

                master.tell(new EndCalculatePi(persistedValue, end - begin), getSelf());
            } else {
                double currentElementValue = 4.0 * (1 - (currentElement % 2) * 2) / (2 * currentElement + 1);
                currentElement++;
                persistor.tell(currentElementValue + persistedValue, getSelf());

                if (currentElement == endElement) {
                    persistor.tell(new RetrieveValue(), getSelf());
                } else {
                    getSelf().tell(new NextElement(), getSelf());
                }
            }
        } else {
            unhandled(o);
        }
    }

}
