package programania.akka.actors;

import akka.actor.UntypedActor;
import org.apache.commons.io.FileUtils;
import programania.akka.messages.RetrieveValue;

import java.io.File;
import java.io.IOException;

public class Persistor extends UntypedActor {

    final File file;

    public Persistor(String filename) {
        this.file = new File(filename);
    }

    @Override
    public void onReceive(Object o) throws Exception {
        if (o instanceof Double) {
            final double value = (double) o;
            writeValue(value);

        } else if (o instanceof RetrieveValue) {
            final double value = readValue();
            getSender().tell(value, getSelf());

        } else {
            unhandled(o);
        }
    }

    private double readValue() throws IOException {
        final String content = file.exists() ? FileUtils.readFileToString(file) : "";
        return content.isEmpty() ? 0.0 : Double.valueOf(content);
    }

    private void writeValue(double value) throws IOException {
        FileUtils.writeStringToFile(file, Double.toString(value));
    }
}
