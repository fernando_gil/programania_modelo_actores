package programania.akka.actors;

import akka.actor.UntypedActor;
import programania.akka.messages.PiFinalResult;

public class Listener extends UntypedActor {

    @Override
    public void onReceive(Object o) throws Exception {
        if (o instanceof PiFinalResult) {
            PiFinalResult finalResult = (PiFinalResult) o;
            System.out.println(String.format("%s terminos: %s (%s mseg)", finalResult.getNumElems(), finalResult.getValue(), finalResult.getMillis()));
        } else {
            unhandled(o);
        }
    }
}
