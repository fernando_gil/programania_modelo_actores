package programania.akka.actors;

import akka.actor.ActorRef;
import akka.actor.Props;
import akka.actor.UntypedActor;
import org.apache.commons.io.FileUtils;
import programania.akka.messages.CalculatePi;
import programania.akka.messages.EndCalculatePi;
import programania.akka.messages.PiFinalResult;
import programania.akka.messages.StartPiCalc;

import java.io.File;
import java.io.IOException;

public class Master extends UntypedActor {

    ActorRef listener;
    int numElems;
    final String filename;

    Master(ActorRef listener) {
        this.listener = listener;
        this.filename = createTempFile();
    }

    @Override
    public void onReceive(Object o) throws Exception {
        if (o instanceof StartPiCalc) {
            StartPiCalc start = (StartPiCalc) o;
            numElems = start.getNrOfElements();

            ActorRef persistor = this.getContext().actorOf(Props.create(Persistor.class, filename));
            ActorRef worker = this.getContext().actorOf(Props.create(Worker.class, getSelf(), persistor));
            worker.tell(new CalculatePi(0, numElems), getSelf());

        } else if (o instanceof EndCalculatePi) {
            EndCalculatePi result = (EndCalculatePi) o;

            listener.tell(new PiFinalResult(numElems, result.getValue(), result.getMillis()), getSelf());
            getContext().stop(getSelf());

            deleteTempFile();

        } else {
            unhandled(o);
        }
    }

    private String createTempFile() {
        try {
            return File.createTempFile("akkaPi", ".tmp").getAbsolutePath();
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    private void deleteTempFile() {
        FileUtils.deleteQuietly(new File(filename));
    }

}
