package programania.akka.actors;

import akka.actor.Actor;
import akka.actor.ActorRef;
import akka.actor.ActorSystem;
import akka.actor.Props;
import akka.testkit.JavaTestKit;
import akka.testkit.TestActorRef;
import org.apache.commons.io.FileUtils;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import programania.akka.messages.RetrieveValue;

import java.io.File;
import java.io.IOException;

import static org.hamcrest.core.Is.is;
import static org.junit.Assert.assertThat;

public class PersistorTest {

    public static final String TMP_FILE = "/tmp/kk";
    private ActorSystem system;

    @Before
    public void setup() {
        this.system = ActorSystem.create("test");
    }

    @After
    public void teardown() {
        JavaTestKit.shutdownActorSystem(system);
        FileUtils.deleteQuietly(new File(TMP_FILE));
    }

    private void populateTestFile(String data) throws IOException {
        FileUtils.writeStringToFile(new File(TMP_FILE), data);
    }

    @Test
    public void actor_almacena_valores() throws IOException {
        TestActorRef<Actor> persistor = TestActorRef.create(system, Props.create(Persistor.class, TMP_FILE));
        persistor.tell(123.45, ActorRef.noSender());

        assertThat(FileUtils.readFileToString(new File(TMP_FILE)), is("123.45"));
    }

    @Test
    public void actor_recupera_valores() throws IOException {
        populateTestFile("678.901");
        final TestActorRef<Actor> persistor = TestActorRef.create(system, Props.create(Persistor.class, TMP_FILE));

        new JavaTestKit(system) {{
            persistor.tell(new RetrieveValue(), getRef());  //ref es el propio JavaTestKit
            expectMsgEquals(678.901);                       //la asercion se hace en base a lo que ref recibe
        }};
    }

    @Test public void actor_trata_correctamente_archivo_vacio() throws IOException {
        populateTestFile("");

        final TestActorRef<Actor> persistor = TestActorRef.create(system, Props.create(Persistor.class, TMP_FILE));

        new JavaTestKit(system) {{
            persistor.tell(new RetrieveValue(), getRef());
            expectMsgEquals(0.0);
        }};
    }
}
