package programania.akka;

import java.util.concurrent.Callable;

class Worker implements Callable<Double> {
    final int start;
    final int nrOfElemens;
    final String filename;

    Worker(int start, int nrOfElemens, String filename) {
        this.start = start;
        this.nrOfElemens = nrOfElemens;
        this.filename = filename;
    }

    @Override
    public Double call() throws Exception {
        calculatePiFor(start, nrOfElemens);
        return Persistor.readStoredValue(filename);
    }

    void calculatePiFor(int start, int nrOfElements) {
        for (int i = start; i < start + nrOfElements; i++) {
            final double value = 4.0 * (1 - (i % 2) * 2) / (2 * i + 1);
            Persistor.accumulateResultOnFile(value, filename);
        }
    }
}
