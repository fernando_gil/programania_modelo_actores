package programania.akka;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

public class ThreadedPi {

    public static void main(String[] args) {
        waitForBegin();

        long mainBegin = System.currentTimeMillis();
        for (int i = 0; i < 1E6; i += 1E5) {
            threadedCalculatePiFor(i, getNrOfSlices());
        }
        long mainEnd = System.currentTimeMillis();

        System.out.println(String.format("main() ejecutado en %s mseg", mainEnd - mainBegin));
    }

    private static void waitForBegin() {
        try {
            System.out.println("Pulse cualquier tecla para comenzar");
            System.in.read();
        } catch (IOException e) {
            //ignorar
        }
    }

    static void threadedCalculatePiFor(int numElems, int nrOfSlices) {
        final ExecutorService executor = Executors.newFixedThreadPool(nrOfSlices);
        final String filename = Persistor.createTempFile();

        try {
            final long begin = System.currentTimeMillis();
            final ArrayList<Worker> workers = createWorkers(numElems, nrOfSlices, filename);
            final List<Future<Double>> results = startWorkers(executor, workers);
            final long end = System.currentTimeMillis();
            printResult(numElems, results, end - begin);
        } catch (InterruptedException | ExecutionException e) {
            e.printStackTrace();
            throw new RuntimeException(e);
        } finally {
            Persistor.deleteTempFile(filename);
            executor.shutdown();
        }
    }

    static int getNrOfSlices() {
        return Runtime.getRuntime().availableProcessors();
    }

    private static ArrayList<Worker> createWorkers(int numElems, int nrOfSlices, String filename) {
        final ArrayList<Worker> toExec = new ArrayList<Worker>();
        final int step = numElems / nrOfSlices;
        for (int j = 0; j < numElems; j += step) {
            toExec.add(new Worker(j, step, filename));
        }
        return toExec;
    }

    private static List<Future<Double>> startWorkers(ExecutorService executor, ArrayList<Worker> workers) throws InterruptedException {
        return executor.invokeAll(workers);
    }

    private static void printResult(int numElems, List<Future<Double>> results, long time) throws ExecutionException, InterruptedException {
        double lastValue = 0.0;
        for (Future<Double> future : results) {
            //cada futuro ira devolviendo una aproximacion mejor, nos quedamos con la ultima
            //asi tambien nos aseguramos de que todos han terminado
            lastValue = future.get();
        }
        System.out.println(String.format("%s terminos: %s (%s mseg)", numElems, lastValue, time));
    }

}
