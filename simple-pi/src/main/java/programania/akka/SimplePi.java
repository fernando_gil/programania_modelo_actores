package programania.akka;

import java.io.IOException;

public class SimplePi {

    public static void main(String[] args) {
        waitForBegin();

        long mainBegin = System.currentTimeMillis();
        for (int i = 0; i < 1E6; i += 1E5) {
            long start = System.currentTimeMillis();
            double res = calculatePiFor(i);
            long end = System.currentTimeMillis();

            System.out.println(String.format("%s terminos: %s (%s mseg)", i, res, end - start));
        }
        long mainEnd = System.currentTimeMillis();

        System.out.println(String.format("main() ejecutado en %s mseg", mainEnd - mainBegin));
    }

    private static void waitForBegin() {
        try {
            System.out.println("Pulse cualquier tecla para comenzar");
            System.in.read();
        } catch (IOException e) {
            //ignorar
        }
    }

    /**
     * Obtiene la aproximacion de pi utilizando la Serie de Leibniz.
     *
     * @param nrOfElements Cuantos terminos de la serie se calcularan.
     * @return Aproximacion de Pi.
     */
    static double calculatePiFor(int nrOfElements) {
        final String filename = Persistor.createTempFile();

        try {
            for (int i = 0; i < nrOfElements; i++) {
                double value = 4.0 * (1 - (i % 2) * 2) / (2 * i + 1);
                Persistor.accumulateResultOnFile(value, filename);
            }

            return Persistor.readStoredValue(filename);
        } finally {
            Persistor.deleteTempFile(filename);
        }
    }
}
