package programania.akka;

import org.apache.commons.io.FileUtils;

import java.io.File;
import java.io.IOException;

class Persistor {

    static String createTempFile() {
        try {
            return File.createTempFile("simplePi", ".tmp").getAbsolutePath();
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    static void deleteTempFile(String filename) {
        FileUtils.deleteQuietly(new File(filename));
    }

    static void accumulateResultOnFile(double value, String filename) {
        double previousValue = readStoredValue(filename);
        writeNewValue(filename, value + previousValue);
    }

    static double readStoredValue(String filename) {
        try {
            final String content = FileUtils.readFileToString(new File(filename));
            return Double.valueOf(content);
        } catch (Exception e) {
            return 0.0;
        }
    }

    static void writeNewValue(String filename, double value) {
        try {
            FileUtils.writeStringToFile(new File(filename), Double.toString(value));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
