package programania.akka;


import akka.actor.ActorRef;
import akka.actor.ActorSystem;
import akka.actor.Props;
import akka.testkit.JavaTestKit;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.io.PrintStream;

import static org.mockito.Mockito.verify;

public class HelloWorldTest {

    @Mock
    PrintStream fakeOut;

    ActorSystem system;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        system = ActorSystem.create();
    }

    @After
    public void teardown() {
        JavaTestKit.shutdownActorSystem(system);
    }

    @Test
    public void hola_mundo() {
        System.setOut(fakeOut);

        new JavaTestKit(system) {{
            ActorRef greeter = system.actorOf(Props.create(HelloWorld.GreetingActor.class));
            greeter.tell("YoMismo", getRef());

            verify(fakeOut).println("Hello YoMismo!");
        }};
    }

    @Test
    public void demo_unhandled() {
        new JavaTestKit(system) {{
            ActorRef greeter = system.actorOf(Props.create(HelloWorld.GreetingActor.class));
            greeter.tell(new Object(), getRef());

            expectNoMsg();
        }};
    }
}
