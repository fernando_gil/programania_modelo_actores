package programania.akka;

import akka.actor.ActorRef;
import akka.actor.ActorSystem;
import akka.actor.Props;
import akka.actor.UntypedActor;

public class HelloWorld {
    public static class GreetingActor extends UntypedActor {

        @Override
        public void onReceive(Object o) throws Exception {
            if (o instanceof String) {
                System.out.println("Hello " + o + "!");
            } else {
                unhandled(o);
            }
        }
    }

    public static void main(String[] args) {
        ActorSystem system = ActorSystem.create("HelloWorld");

        ActorRef greetingActor = system.actorOf(Props.create(GreetingActor.class), "greeter");

        ActorRef greetingActor2 = system.actorFor("/user/greeter");
        greetingActor2.tell("Valladolid", ActorRef.noSender());

        system.stop(greetingActor);
        system.shutdown();
    }
}
